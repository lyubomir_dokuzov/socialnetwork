package com.sample.socialnetwork.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.di.annotation.ViewModelKey;
import com.sample.socialnetwork.home.SocialNetworkViewModel;
import com.sample.socialnetwork.home.album.AlbumViewModel;
import com.sample.socialnetwork.home.album.preview.AlbumPreviewViewModel;
import com.sample.socialnetwork.home.feed.FeedViewModel;
import com.sample.socialnetwork.home.note.NotesViewModel;
import com.sample.socialnetwork.home.user.UserDetailsViewModel;
import com.sample.socialnetwork.home.user.UsersListViewModel;
import com.sample.socialnetwork.login.LoginViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Lyubomir Dokuzov
 */
@Module
public abstract class ViewModelModule {

    public ViewModelModule() {
    }

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(SocialNetworkViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract ViewModel bindLoginViewModel(LoginViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SocialNetworkViewModel.class)
    abstract ViewModel bindSocialNetworkViewModel(SocialNetworkViewModel userViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel.class)
    abstract ViewModel bindFeedViewModel(FeedViewModel feedViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AlbumViewModel.class)
    abstract ViewModel bindAlbumViewModel(AlbumViewModel albumViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(AlbumPreviewViewModel.class)
    abstract ViewModel bindAlbumPreviewViewModel(AlbumPreviewViewModel albumPreviewViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NotesViewModel.class)
    abstract ViewModel bindNotesViewModel(NotesViewModel notesViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel.class)
    abstract ViewModel bindUsersListViewModel(UsersListViewModel usersListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel.class)
    abstract ViewModel bindUserDetailsViewModel(UserDetailsViewModel userDetailsViewModel);

}
