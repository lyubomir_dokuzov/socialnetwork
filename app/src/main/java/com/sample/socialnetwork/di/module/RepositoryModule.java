package com.sample.socialnetwork.di.module;

import com.sample.socialnetwork.home.album.AlbumRepository;
import com.sample.socialnetwork.home.album.AlbumRepositoryImpl;
import com.sample.socialnetwork.home.album.preview.PhotosRepository;
import com.sample.socialnetwork.home.album.preview.PhotosRepositoryImpl;
import com.sample.socialnetwork.home.feed.FeedRepository;
import com.sample.socialnetwork.home.feed.FeedRepositoryImpl;
import com.sample.socialnetwork.home.note.NotesRepository;
import com.sample.socialnetwork.home.note.NotesRepositoryImpl;
import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.login.UserRepositoryImpl;
import com.sample.socialnetwork.network.api.SocialNetworkApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lyubomir Dokuzov
 */
@Module
public class RepositoryModule {

    public RepositoryModule() {

    }

    @Provides
    @Singleton
    UserRepository providesUserRepository(SocialNetworkApi socialNetworkApi) {
        return new UserRepositoryImpl(socialNetworkApi);
    }

    @Provides
    @Singleton
    FeedRepository providesFeedRepository(SocialNetworkApi socialNetworkApi) {
        return new FeedRepositoryImpl(socialNetworkApi);
    }

    @Provides
    @Singleton
    AlbumRepository providesAlbumRepository(SocialNetworkApi socialNetworkApi) {
        return new AlbumRepositoryImpl(socialNetworkApi);
    }

    @Provides
    @Singleton
    PhotosRepository providesPhotosRepository(SocialNetworkApi socialNetworkApi) {
        return new PhotosRepositoryImpl(socialNetworkApi);
    }

    @Provides
    @Singleton
    NotesRepository providesNotesRepository(SocialNetworkApi socialNetworkApi) {
        return new NotesRepositoryImpl(socialNetworkApi);
    }


}
