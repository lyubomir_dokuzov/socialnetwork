package com.sample.socialnetwork.di;

/**
 * Created by Lyubomir Dokuzov
 */

public interface HasComponent<C> {
    C getComponent();
}
