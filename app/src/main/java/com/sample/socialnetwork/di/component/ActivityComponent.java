package com.sample.socialnetwork.di.component;

import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.di.module.ActivityModule;
import com.sample.socialnetwork.di.scope.PerActivity;
import com.sample.socialnetwork.home.SocialNetworkActivity;
import com.sample.socialnetwork.home.album.AlbumFragment;
import com.sample.socialnetwork.home.album.preview.AlbumPreviewActivity;
import com.sample.socialnetwork.home.feed.FeedFragment;
import com.sample.socialnetwork.home.note.NotesFragment;
import com.sample.socialnetwork.home.user.UserDetailsActivity;
import com.sample.socialnetwork.home.user.UserListActivity;
import com.sample.socialnetwork.login.LoginActivity;

import dagger.Subcomponent;

/**
 * Created by Lyubomir Dokuzov
 */

@PerActivity
@Subcomponent(modules = {
        ActivityModule.class,
})
public interface ActivityComponent {
    void inject(LoginActivity loginActivity);

    void inject(SocialNetworkActivity socialNetworkActivity);

    void inject(BaseActivity baseActivity);

    void inject(UserListActivity baseActivity);

    void inject(AlbumPreviewActivity baseActivity);

    void inject(FeedFragment feedFragment);

    void inject(AlbumFragment albumFragment);

    void inject(NotesFragment notesFragment);

    void inject(UserDetailsActivity userDetailFragment);
}
