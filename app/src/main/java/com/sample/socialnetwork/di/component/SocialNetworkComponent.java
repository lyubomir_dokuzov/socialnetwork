package com.sample.socialnetwork.di.component;

import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.di.module.ActivityModule;
import com.sample.socialnetwork.di.module.NetworkModule;
import com.sample.socialnetwork.di.module.RepositoryModule;
import com.sample.socialnetwork.di.module.SocialNetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Lyubomir Dokuzov
 */
@Singleton
@Component(modules = {
        SocialNetworkModule.class,
        NetworkModule.class,
        RepositoryModule.class
})
public interface SocialNetworkComponent {
    ActivityComponent plus(ActivityModule activityModule);

    void inject(BaseActivity baseActivity);
}
