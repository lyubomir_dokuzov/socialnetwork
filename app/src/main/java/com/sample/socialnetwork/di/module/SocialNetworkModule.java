package com.sample.socialnetwork.di.module;

import android.content.Context;

import com.sample.socialnetwork.SocialNetworkApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Lyubomir Dokuzov
 */

@Module(includes = ViewModelModule.class)
public class SocialNetworkModule {

    private final SocialNetworkApplication app;

    public SocialNetworkModule(SocialNetworkApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    SocialNetworkApplication provideApplication() {
        return app;
    }

    @Provides
    Context provideApplicationContext() {
        return app.getApplicationContext();
    }


}
