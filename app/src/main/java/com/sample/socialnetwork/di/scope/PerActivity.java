package com.sample.socialnetwork.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Lyubomir Dokuzov
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
