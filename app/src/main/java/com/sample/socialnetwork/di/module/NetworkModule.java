package com.sample.socialnetwork.di.module;

import android.content.Context;

import com.sample.socialnetwork.R;
import com.sample.socialnetwork.network.CustomObjectMapper;
import com.sample.socialnetwork.network.api.SocialNetworkApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Lyubomir Dokuzov
 */
@Module
public class NetworkModule {

    public NetworkModule() {

    }


    @Singleton
    @Provides
    CustomObjectMapper providesCustomObjectMapper() {
        return new CustomObjectMapper();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(Context context, CustomObjectMapper objectMapper) {
        return new Retrofit.Builder()
                .client(new OkHttpClient())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .baseUrl(context.getString(R.string.base_url))
                .build();
    }

    @Singleton
    @Provides
    SocialNetworkApi provideSocialNetworkApi(Retrofit retrofit) {
        return retrofit.create(SocialNetworkApi.class);
    }

}
