package com.sample.socialnetwork.di.module;

import android.app.Activity;

import dagger.Module;

/**
 * Created by Lyubomir Dokuzov
 */

@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }


}
