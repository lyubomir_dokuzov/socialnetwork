package com.sample.socialnetwork.base;

import android.arch.lifecycle.LifecycleActivity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.inputmethod.InputMethodManager;

import com.sample.socialnetwork.SocialNetworkApplication;
import com.sample.socialnetwork.di.HasComponent;
import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.di.component.ActivityComponent;
import com.sample.socialnetwork.di.component.SocialNetworkComponent;
import com.sample.socialnetwork.di.module.ActivityModule;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class BaseActivity extends LifecycleActivity implements HasComponent<ActivityComponent> {
    @Inject
    protected SocialNetworkViewModelFactory viewModelFactory;

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = getAppComponent(this).plus(new ActivityModule(this));
        getComponent().inject(this);
    }

    @Override
    public ActivityComponent getComponent() {
        return activityComponent;
    }

    protected SocialNetworkComponent getAppComponent(Context context) {
        return SocialNetworkApplication.getAppComponent(context);
    }

    public void hideKeyboard() {
        if (!isFinishing()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }



}
