package com.sample.socialnetwork.base;

import android.arch.lifecycle.LifecycleFragment;

import com.sample.socialnetwork.di.HasComponent;
import com.sample.socialnetwork.di.component.ActivityComponent;

/**
 * Created by Lyubomir Dokuzov
 */

public class BaseFragment extends LifecycleFragment {


    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    protected ActivityComponent getActivityComponent() {
        return getComponent(ActivityComponent.class);
    }

    protected void hideKeyboard() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).hideKeyboard();
        }
    }
}
