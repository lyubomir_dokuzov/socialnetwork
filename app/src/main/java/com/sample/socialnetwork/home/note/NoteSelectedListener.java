package com.sample.socialnetwork.home.note;

import com.sample.socialnetwork.network.model.Note;

/**
 * Created by Lyubomir Dokuzov
 */

public interface NoteSelectedListener {
    void onNoteSelected(Note note, int position);
}
