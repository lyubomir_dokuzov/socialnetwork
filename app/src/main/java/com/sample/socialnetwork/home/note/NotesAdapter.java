package com.sample.socialnetwork.home.note;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sample.socialnetwork.databinding.LayoutNotesListItemBinding;
import com.sample.socialnetwork.network.model.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public class NotesAdapter extends RecyclerView.Adapter {

    private final NoteSelectedListener listener;
    private List<Note> notes = new ArrayList<>();

    public NotesAdapter(NoteSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LayoutNotesListItemBinding binding = LayoutNotesListItemBinding.inflate(inflater, parent, false);
        return new NotesVH(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotesVH) {
            ((NotesVH) holder).bind(notes.get(position), position == notes.size() - 1, listener, position);
        }
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    public void itemRemoved(Note note, int position) {
        notes.remove(note);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, notes.size());
    }

    public static class NotesVH extends RecyclerView.ViewHolder {
        private final LayoutNotesListItemBinding binding;

        public NotesVH(LayoutNotesListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Note note, boolean isLast, NoteSelectedListener listener, int position) {
            binding.setModel(note);
            binding.setIsLast(isLast);
            binding.post.setOnLongClickListener(view -> {
                listener.onNoteSelected(binding.getModel(), position);
                return true;
            });
            binding.executePendingBindings();
        }
    }
}
