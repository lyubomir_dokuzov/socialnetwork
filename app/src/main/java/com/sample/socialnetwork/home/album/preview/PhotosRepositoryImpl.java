package com.sample.socialnetwork.home.album.preview;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sample.socialnetwork.network.LoggableResponseCallback;
import com.sample.socialnetwork.network.api.SocialNetworkApi;
import com.sample.socialnetwork.network.model.Photo;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */

public class PhotosRepositoryImpl implements PhotosRepository {


    private final SocialNetworkApi socialNetworkApi;

    private final MutableLiveData<List<Photo>> photos = new MutableLiveData<>();

    @Inject
    public PhotosRepositoryImpl(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    @Override
    public LiveData<List<Photo>> getPhotos(String albumId) {

        if (photos.getValue() == null) {
            fetchPhotos(albumId);
        }
        return photos;
    }

    private void fetchPhotos(String albumId) {
        socialNetworkApi.getAlbumPhotos(albumId).enqueue(new LoggableResponseCallback<List<Photo>>() {
            @Override
            public void onResponse(@NonNull Call<List<Photo>> call, @NonNull Response<List<Photo>> response) {
                super.onResponse(call, response);
                photos.setValue(response.body());
            }
        });
    }
}
