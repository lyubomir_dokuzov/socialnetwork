package com.sample.socialnetwork.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.User;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */
public class SocialNetworkViewModel extends ViewModel {

    private final UserRepository userRepository;

    @Inject
    public SocialNetworkViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public LiveData<User> getCurrentUser() {
        return userRepository.getCurrentlyLoggedUser();
    }


}
