package com.sample.socialnetwork.home.album;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.Album;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumViewModel extends ViewModel {


    private final AlbumRepository albumRepository;
    private final UserRepository userRepository;

    @Inject
    public AlbumViewModel(AlbumRepository albumRepository, UserRepository userRepository) {
        this.albumRepository = albumRepository;
        this.userRepository = userRepository;
    }

    public LiveData<List<Album>> getAlbums() {
        return albumRepository.getUserAlbums(userRepository.getCurrentlyLoggedUser().getValue().getId());
    }
}
