package com.sample.socialnetwork.home.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.User;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class UserDetailsViewModel extends ViewModel {

    private final UserRepository userRepository;

    @Inject
    public UserDetailsViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public LiveData<User> getCurrentUser() {
        return userRepository.getCurrentlyLoggedUser();
    }

    public LiveData<Result> updateUser(User user) {
        return userRepository.updateUser(user);
    }
}
