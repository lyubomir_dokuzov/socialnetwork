package com.sample.socialnetwork.home.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.User;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class UsersListViewModel extends ViewModel {

    private final UserRepository userRepository;

    @Inject
    public UsersListViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public LiveData<User> getCurrentUser() {
        return userRepository.getCurrentlyLoggedUser();
    }

    public LiveData<List<User>> getAllUsers() {
        return userRepository.getUsers();
    }
}
