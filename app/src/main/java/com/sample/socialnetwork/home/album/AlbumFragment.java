package com.sample.socialnetwork.home.album;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sample.socialnetwork.Constant;
import com.sample.socialnetwork.base.BaseFragment;
import com.sample.socialnetwork.databinding.FragmentAlbumBinding;
import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.home.album.preview.AlbumPreviewActivity;
import com.sample.socialnetwork.network.model.Album;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumFragment extends BaseFragment implements AlbumSelectedListener {

    public static final int ALBUM_SPAN_COUNT = 2;

    @Inject
    protected SocialNetworkViewModelFactory viewModelFactory;

    private FragmentAlbumBinding dataBinding;
    private AlbumViewModel viewModel;
    private AlbumAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceStat) {

        getActivityComponent().inject(this);

        dataBinding = FragmentAlbumBinding.inflate(inflater, container, false);

        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AlbumViewModel.class);

        if (adapter == null) {
            adapter = new AlbumAdapter(this);
        }

        dataBinding.recycler.setAdapter(adapter);
        viewModel.getAlbums().observe(this, albums -> adapter.setAlbums(albums));
        dataBinding.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    @Override
    public void onAlbumSelected(Album album) {
        Intent intent = new Intent(getActivity(), AlbumPreviewActivity.class);
        intent.putExtra(Constant.BundleKey.ALBUM_ID, album.getId());
        intent.putExtra(Constant.BundleKey.ALBUM_TITLE, album.getTitle());
        startActivity(intent);
    }
}
