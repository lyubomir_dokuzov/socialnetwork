package com.sample.socialnetwork.home.feed;

import com.sample.socialnetwork.network.model.Post;

/**
 * Created by Lyubomir Dokuzov
 */

public interface PostSelectedListener {
    void onPostSelected(Post post);
}
