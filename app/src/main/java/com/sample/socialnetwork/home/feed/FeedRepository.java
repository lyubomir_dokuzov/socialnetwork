package com.sample.socialnetwork.home.feed;

import android.arch.lifecycle.LiveData;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.model.Post;

import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public interface FeedRepository {

    LiveData<List<Post>> getUserPosts(String userId);

    LiveData<Result> post(Post post);

    LiveData<Result> deletePost(String postId);
}
