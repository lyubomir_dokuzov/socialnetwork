package com.sample.socialnetwork.home.album;

import com.sample.socialnetwork.network.model.Album;

/**
 * Created by Lyubomir Dokuzov
 */

public interface AlbumSelectedListener {
    void onAlbumSelected(Album album);
}
