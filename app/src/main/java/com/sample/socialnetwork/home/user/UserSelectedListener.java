package com.sample.socialnetwork.home.user;

import com.sample.socialnetwork.network.model.User;

/**
 * Created by Lyubomir Dokuzov
 */

public interface UserSelectedListener {
    void onUserSelected(User user);
}
