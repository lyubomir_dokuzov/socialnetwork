package com.sample.socialnetwork.home.album.preview;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.network.model.Photo;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumPreviewViewModel extends ViewModel {

    private final PhotosRepository photoRepository;

    @Inject
    public AlbumPreviewViewModel(PhotosRepository photosRepository) {
        this.photoRepository = photosRepository;
    }

    public LiveData<List<Photo>> getPhotos(String albumId) {
        return photoRepository.getPhotos(albumId);
    }


}
