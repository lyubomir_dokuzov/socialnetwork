package com.sample.socialnetwork.home.user;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import com.sample.socialnetwork.Constant;
import com.sample.socialnetwork.R;
import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.databinding.ActivityUserListBinding;
import com.sample.socialnetwork.network.model.User;

import org.parceler.Parcels;

/**
 * Created by Lyubomir Dokuzov
 */

public class UserListActivity extends BaseActivity implements UserSelectedListener {

    private ActivityUserListBinding binding;
    private UsersListViewModel viewModel;
    private UsersAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_list);

        if (adapter == null) {
            adapter = new UsersAdapter(this);
        }

        binding.recyclerUsers.setAdapter(adapter);
        binding.recyclerUsers.setLayoutManager(new LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UsersListViewModel.class);
        binding.currentUser.content.setOnClickListener(view -> navigateToUserDetails(binding.getCurrentUser()));

        viewModel.getCurrentUser().observe(this, user -> binding.setCurrentUser(user));
        viewModel.getAllUsers().observe(this, users -> adapter.setUsers(users));
    }


    @Override
    public void onUserSelected(User user) {
        navigateToUserDetails(user);
    }

    private void navigateToUserDetails(User user) {
        Intent intent = new Intent(this, UserDetailsActivity.class);
        intent.putExtra(Constant.BundleKey.USER, Parcels.wrap(user));
        startActivity(intent);
    }
}
