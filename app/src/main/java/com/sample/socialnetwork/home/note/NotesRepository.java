package com.sample.socialnetwork.home.note;

import android.arch.lifecycle.LiveData;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.model.Note;

import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public interface NotesRepository {
    LiveData<List<Note>> getUserNotes(String userId);

    LiveData<Result> removeNote(Note note);

    LiveData<Result> addNote(Note note);
}
