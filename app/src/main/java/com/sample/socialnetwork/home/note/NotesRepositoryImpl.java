package com.sample.socialnetwork.home.note;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.LoggableResponseCallback;
import com.sample.socialnetwork.network.api.SocialNetworkApi;
import com.sample.socialnetwork.network.model.Note;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */

public class NotesRepositoryImpl implements NotesRepository {

    private final SocialNetworkApi socialNetworkApi;
    private final MutableLiveData<List<Note>> notes = new MutableLiveData<>();

    @Inject
    public NotesRepositoryImpl(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    @Override
    public LiveData<List<Note>> getUserNotes(String userId) {
        if (notes.getValue() == null) {
            fetchNotes(userId);
        }
        return notes;
    }

    @Override
    public LiveData<Result> removeNote(Note note) {
        removeAndUpdatedLocally(note);

        return networkDelete(note);
    }

    @Override
    public LiveData<Result> addNote(Note note) {
        addAndUpdatedLocally(note);

        return networkAdd(note);
    }

    private void removeAndUpdatedLocally(Note note) {
        if (notes.getValue() != null) {
            List<Note> oldVal = notes.getValue();
            oldVal.remove(note);
            notes.setValue(oldVal);
        }
    }


    private void addAndUpdatedLocally(Note note) {
        if (notes.getValue() != null) {
            List<Note> oldVal = notes.getValue();
            oldVal.add(note);
            notes.setValue(oldVal);
        }
    }

    private LiveData<Result> networkAdd(Note note) {
        MutableLiveData<Result> result = new MutableLiveData<>();

        socialNetworkApi.submitUserNote(note).enqueue(new LoggableResponseCallback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                super.onResponse(call, response);
                result.setValue(Result.SUCESS);
            }
        });
        return result;
    }

    private LiveData<Result> networkDelete(Note note) {
        MutableLiveData<Result> result = new MutableLiveData<>();

        socialNetworkApi.deleteUserNote(note.getUserId()).enqueue(new LoggableResponseCallback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                super.onResponse(call, response);
                result.setValue(Result.SUCESS);
            }
        });
        return result;
    }

    private void fetchNotes(String userId) {
        socialNetworkApi.getUserNotes(userId).enqueue(new LoggableResponseCallback<List<Note>>() {
            @Override
            public void onResponse(@NonNull Call<List<Note>> call, @NonNull Response<List<Note>> response) {
                super.onResponse(call, response);
                notes.setValue(response.body());
            }
        });
    }
}
