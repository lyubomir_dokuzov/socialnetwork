package com.sample.socialnetwork.home.note;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.base.BaseFragment;
import com.sample.socialnetwork.databinding.FragmentNotesBinding;
import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.network.model.Note;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class NotesFragment extends BaseFragment implements NoteSelectedListener {


    @Inject
    protected SocialNetworkViewModelFactory viewModelFactory;

    private NotesViewModel notesViewModel;

    private FragmentNotesBinding dataBinding;
    private NotesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceStat) {

        getActivityComponent().inject(this);

        dataBinding = FragmentNotesBinding.inflate(inflater, container, false);

        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        notesViewModel = ViewModelProviders.of(this, viewModelFactory).get(NotesViewModel.class);

        if (adapter == null) {
            adapter = new NotesAdapter(this);
        }

        dataBinding.recyclerNotes.setAdapter(adapter);
        dataBinding.recyclerNotes.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataBinding.buttonAdd.setOnClickListener(view -> addNote());

        notesViewModel.getUserNotes().observe(this, notes -> adapter.setNotes(notes));
    }

    @Override
    public void onNoteSelected(final Note note, final int position) {
        notesViewModel.removeNote(note).observe(this, result -> {
            if (result == Result.SUCESS) {
                Toast.makeText(getActivity(), "Deleted " + note.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addNote() {
        String title = dataBinding.etNoteTitle.getText().toString();
        notesViewModel.addNote(title).observe(this, result -> {
            if (result == Result.SUCESS) {
                Toast.makeText(getActivity(), "Note added - " + title, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
