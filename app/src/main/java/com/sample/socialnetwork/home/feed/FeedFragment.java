package com.sample.socialnetwork.home.feed;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.base.BaseFragment;
import com.sample.socialnetwork.databinding.FragmentFeedBinding;
import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.network.model.Post;

import javax.inject.Inject;

import static android.widget.Toast.LENGTH_SHORT;
import static android.widget.Toast.makeText;

/**
 * Created by Lyubomir Dokuzov
 */

public class FeedFragment extends BaseFragment implements PostSelectedListener {

    @Inject
    protected SocialNetworkViewModelFactory viewModelFactory;

    private FeedViewModel feedViewModel;
    /// TODO run profiling, maybe databinding && adapter need to be wrapped in AutoClearedValue
    private FragmentFeedBinding dataBinding;
    private FeedAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceStat) {

        getActivityComponent().inject(this);

        dataBinding = FragmentFeedBinding.inflate(inflater, container, false);

        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        feedViewModel = ViewModelProviders.of(this, viewModelFactory).get(FeedViewModel.class);

        if (adapter == null) {
            adapter = new FeedAdapter(this);
        }

        dataBinding.recyclerPosts.setAdapter(adapter);
        dataBinding.recyclerPosts.setLayoutManager(new LinearLayoutManager(getActivity()));

        dataBinding.buttonPost.setOnClickListener(view -> submitPost());

        fetchUserPosts();
    }

    @Override
    public void onPostSelected(Post post) {
        // for simplicity i'll omit the prompt dialog and delete directly
        feedViewModel.deletePost(post.getId()).observe(this, result -> {
            if (result == Result.SUCESS) {
                makeText(getActivity(), "Deleted post: " + post.getTitle(), LENGTH_SHORT).show();
                // optionally reload posts || notify item removed
            }
        });
    }

    private void fetchUserPosts() {
        feedViewModel.getUserPosts().observe(this, posts -> adapter.setPosts(posts));
    }

    private void submitPost() {

        final Post post = new Post();
        post.setBody(dataBinding.etPostTitle.getText().toString());
        post.setTitle(dataBinding.etPostBody.getText().toString());
        post.setId(feedViewModel.getCurrentUser().getId());

        feedViewModel.submitPost(post).observe(this, result -> {
            if (result == Result.SUCESS) {
                makeText(getActivity(), "Submitted post: " + post.getTitle(), LENGTH_SHORT).show();
                //  optionally reload posts || notify item inserted
            }
        });
    }
}
