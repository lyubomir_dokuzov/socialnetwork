package com.sample.socialnetwork.home.album.preview;

import android.arch.lifecycle.LiveData;

import com.sample.socialnetwork.network.model.Photo;

import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public interface PhotosRepository {
    LiveData<List<Photo>> getPhotos(String albumId);
}
