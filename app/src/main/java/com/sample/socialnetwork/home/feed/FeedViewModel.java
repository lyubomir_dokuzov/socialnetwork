package com.sample.socialnetwork.home.feed;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.Post;
import com.sample.socialnetwork.network.model.User;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class FeedViewModel extends ViewModel {

    private final UserRepository userRepository;
    private final FeedRepository feedRepository;

    @Inject
    public FeedViewModel(UserRepository userRepository, FeedRepository feedRepository) {
        this.userRepository = userRepository;
        this.feedRepository = feedRepository;
    }

    public User getCurrentUser() {
        return userRepository.getCurrentlyLoggedUser().getValue();
    }

    public LiveData<List<Post>> getUserPosts() {
        return feedRepository.getUserPosts(userRepository.getCurrentlyLoggedUser().getValue().getId());
    }

    public LiveData<Result> submitPost(Post post) {
        return feedRepository.post(post);
    }

    public LiveData<Result> deletePost(String postId) {
        return feedRepository.deletePost(postId);
    }
}
