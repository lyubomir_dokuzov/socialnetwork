package com.sample.socialnetwork.home.user;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sample.socialnetwork.databinding.LayoutUserListItemBinding;
import com.sample.socialnetwork.network.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public class UsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final UserSelectedListener listener;
    private List<User> users = new ArrayList<>();

    public UsersAdapter(UserSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LayoutUserListItemBinding binding = LayoutUserListItemBinding.inflate(inflater, parent, false);
        return new UserListItemVH(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserListItemVH) {
            ((UserListItemVH) holder).bind(users.get(position), position == users.size() - 1, listener);
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @BindingAdapter("visibility")
    public static void setVisibility(final View view, boolean visible) {

        if (view == null) return;

        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public static class UserListItemVH extends RecyclerView.ViewHolder {

        private final LayoutUserListItemBinding binding;

        public UserListItemVH(LayoutUserListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(User model, boolean isLast, UserSelectedListener listener) {
            binding.setModel(model);
            binding.setIsLast(isLast);
            binding.content.setOnClickListener(view -> listener.onUserSelected(model));

            binding.executePendingBindings();
        }
    }
}
