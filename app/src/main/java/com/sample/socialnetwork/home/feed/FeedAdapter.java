package com.sample.socialnetwork.home.feed;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sample.socialnetwork.databinding.LayoutFeedListItemBinding;
import com.sample.socialnetwork.network.model.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public class FeedAdapter extends RecyclerView.Adapter {


    private final PostSelectedListener listener;
    private List<Post> posts = new ArrayList<>();

    public FeedAdapter(PostSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LayoutFeedListItemBinding binding = LayoutFeedListItemBinding.inflate(inflater, parent, false);
        return new PostVH(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostVH) {
            ((PostVH) holder).bind(posts.get(position), position == posts.size() - 1, listener);
        }
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    public static class PostVH extends RecyclerView.ViewHolder {
        private final LayoutFeedListItemBinding binding;

        public PostVH(LayoutFeedListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Post post, boolean isLast, PostSelectedListener listener) {
            binding.setModel(post);
            binding.setIsLast(isLast);
            binding.post.setOnLongClickListener(view -> {
                listener.onPostSelected(binding.getModel());
                return true;
            });
            binding.executePendingBindings();
        }
    }
}
