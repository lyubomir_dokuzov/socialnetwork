package com.sample.socialnetwork.home.album;

import android.arch.lifecycle.LiveData;

import com.sample.socialnetwork.network.model.Album;

import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public interface AlbumRepository {

    LiveData<List<Album>> getUserAlbums(String userId);
}
