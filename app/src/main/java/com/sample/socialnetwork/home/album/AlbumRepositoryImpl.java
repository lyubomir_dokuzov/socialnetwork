package com.sample.socialnetwork.home.album;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.sample.socialnetwork.network.LoggableResponseCallback;
import com.sample.socialnetwork.network.api.SocialNetworkApi;
import com.sample.socialnetwork.network.model.Album;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumRepositoryImpl implements AlbumRepository {


    private final SocialNetworkApi socialNetworkApi;
    private final MutableLiveData<List<Album>> albums = new MutableLiveData<>();

    public AlbumRepositoryImpl(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    @Override
    public LiveData<List<Album>> getUserAlbums(String userId) {
        if (albums.getValue() == null) {
            fetchAlbums(userId);
        }
        return albums;
    }

    private void fetchAlbums(String userId) {
        socialNetworkApi.getUserAlbums(userId).enqueue(new LoggableResponseCallback<List<Album>>() {
            @Override
            public void onResponse(@NonNull Call<List<Album>> call, @NonNull Response<List<Album>> response) {
                super.onResponse(call, response);
                albums.setValue(response.body());
            }
        });
    }
}
