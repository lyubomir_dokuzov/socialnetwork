package com.sample.socialnetwork.home;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.sample.socialnetwork.R;
import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.databinding.ActivitySocialNetworkBinding;
import com.sample.socialnetwork.home.album.AlbumFragment;
import com.sample.socialnetwork.home.feed.FeedFragment;
import com.sample.socialnetwork.home.note.NotesFragment;
import com.sample.socialnetwork.home.user.UserListActivity;


public class SocialNetworkActivity extends BaseActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    private SocialNetworkViewModel viewModel;
    private ActivitySocialNetworkBinding binding;
    private MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_social_network);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SocialNetworkViewModel.class);

        viewModel.getCurrentUser().observe(this, loggedInUser -> binding.setModel(loggedInUser));

        binding.toolbar.setOnClickListener(view -> startActivity(new Intent(this, UserListActivity.class)));
        binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
        binding.viewpager.addOnPageChangeListener(this);
        setupViewPager();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_feed:
                binding.viewpager.setCurrentItem(0);
                break;
            case R.id.item_album:
                binding.viewpager.setCurrentItem(1);
                break;
            case R.id.item_todo:
                binding.viewpager.setCurrentItem(2);
                break;
        }

        return false;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if (prevMenuItem != null) {
            prevMenuItem.setChecked(false);
        } else {
            binding.bottomNavigation.getMenu().getItem(0).setChecked(false);
        }

        binding.bottomNavigation.getMenu().getItem(position).setChecked(true);
        prevMenuItem = binding.bottomNavigation.getMenu().getItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setupViewPager() {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new FeedFragment());
        adapter.addFragment(new AlbumFragment());
        adapter.addFragment(new NotesFragment());

        binding.viewpager.setAdapter(adapter);
    }
}
