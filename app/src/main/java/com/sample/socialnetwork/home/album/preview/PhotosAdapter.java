package com.sample.socialnetwork.home.album.preview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sample.socialnetwork.databinding.LayoutPhotoListItemBinding;
import com.sample.socialnetwork.network.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public class PhotosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Photo> photos = new ArrayList<>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LayoutPhotoListItemBinding binding = LayoutPhotoListItemBinding.inflate(inflater, parent, false);
        return new PhotoVH(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PhotoVH) {
            ((PhotoVH) holder).bind(photos.get(position).getThumbnailUrl());
        }
    }


    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }

    public static class PhotoVH extends RecyclerView.ViewHolder {

        private final LayoutPhotoListItemBinding binding;

        public PhotoVH(LayoutPhotoListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(String url) {
            Picasso.with(binding.image.getContext()).load(url).into(binding.image);
        }

    }


}
