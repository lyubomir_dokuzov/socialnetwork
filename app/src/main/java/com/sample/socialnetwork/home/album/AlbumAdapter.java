package com.sample.socialnetwork.home.album;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sample.socialnetwork.databinding.LayoutAlbumListItemBinding;
import com.sample.socialnetwork.network.model.Album;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final AlbumSelectedListener listener;
    private List<Album> albums = new ArrayList<>();

    public AlbumAdapter(AlbumSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LayoutAlbumListItemBinding binding = LayoutAlbumListItemBinding.inflate(inflater, parent, false);
        return new AlbumVH(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AlbumVH) {
            ((AlbumVH) holder).bind(albums.get(position), position == albums.size() - 1, listener);
        }
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    public static class AlbumVH extends RecyclerView.ViewHolder {

        private final LayoutAlbumListItemBinding binding;

        public AlbumVH(LayoutAlbumListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Album album, boolean isLast, AlbumSelectedListener listener) {
            binding.setModel(album);
            binding.setIsLast(isLast);
            binding.item.setOnClickListener(view -> listener.onAlbumSelected(binding.getModel()));
            binding.executePendingBindings();
        }
    }
}
