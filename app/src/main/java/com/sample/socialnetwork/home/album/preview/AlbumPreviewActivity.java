package com.sample.socialnetwork.home.album.preview;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;

import com.sample.socialnetwork.Constant;
import com.sample.socialnetwork.R;
import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.databinding.ActivityAlbumPreviewBinding;

/**
 * Created by Lyubomir Dokuzov
 */

public class AlbumPreviewActivity extends BaseActivity {

    public static final int PHOTO_GRID_SPAN_COUNT = 4;


    private AlbumPreviewViewModel viewModel;

    private ActivityAlbumPreviewBinding binding;
    private PhotosAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_album_preview);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AlbumPreviewViewModel.class);
        binding.toolbar.setTitle(getAlbumTitle());


        if (adapter == null) {
            adapter = new PhotosAdapter();
        }
        binding.recyclerPhotos.setAdapter(adapter);
        binding.recyclerPhotos.setLayoutManager(new GridLayoutManager(this, PHOTO_GRID_SPAN_COUNT));

        viewModel.getPhotos(getAlbumId()).observe(this, photos -> adapter.setPhotos(photos));

    }

    private String getAlbumId() {
        String albumId = "";

        Bundle arg = getIntent().getExtras();
        if (arg != null && arg.containsKey(Constant.BundleKey.ALBUM_ID)) {
            albumId = arg.getString(Constant.BundleKey.ALBUM_ID);
        }

        return albumId;
    }

    public String getAlbumTitle() {
        String albumId = "";

        Bundle arg = getIntent().getExtras();
        if (arg != null && arg.containsKey(Constant.BundleKey.ALBUM_ID)) {
            albumId = arg.getString(Constant.BundleKey.ALBUM_TITLE);
        }

        return albumId;
    }
}
