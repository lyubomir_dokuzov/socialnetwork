package com.sample.socialnetwork.home.note;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.login.UserRepository;
import com.sample.socialnetwork.network.model.Note;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class NotesViewModel extends ViewModel {

    private final NotesRepository notesRepository;
    private final UserRepository userRepository;

    @Inject
    public NotesViewModel(NotesRepository notesRepository, UserRepository userRepository) {
        this.notesRepository = notesRepository;
        this.userRepository = userRepository;
    }

    public LiveData<List<Note>> getUserNotes() {
        return notesRepository.getUserNotes(userRepository.getCurrentlyLoggedUser().getValue().getId());
    }

    public LiveData<Result> removeNote(Note note) {
        return notesRepository.removeNote(note);
    }

    public LiveData<Result> addNote(String title) {
        final Note note = new Note();
        note.setUserId(userRepository.getCurrentlyLoggedUser().getValue().getId());
        note.setTitle(title);
        note.setCompleted(false); // no ui options for completion mark

        return notesRepository.addNote(note);
    }
}
