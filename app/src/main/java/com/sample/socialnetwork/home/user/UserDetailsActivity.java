package com.sample.socialnetwork.home.user;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.sample.socialnetwork.Constant;
import com.sample.socialnetwork.R;
import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.databinding.FragmentUserDetailsBinding;
import com.sample.socialnetwork.di.SocialNetworkViewModelFactory;
import com.sample.socialnetwork.network.model.User;

import org.parceler.Parcels;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class UserDetailsActivity extends BaseActivity {

    @Inject
    protected SocialNetworkViewModelFactory viewModelFactory;

    private FragmentUserDetailsBinding binding;
    private UserDetailsViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_user_details);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserDetailsViewModel.class);
        binding.setModel(getUser());
        binding.setIsEditable(isEditable());

        binding.save.setOnClickListener(view -> updateUser());
    }

    private void updateUser() {
        final User user = new User();
        user.setName(binding.etName.getText().toString());
        user.setName(binding.etEmail.getText().toString());

        viewModel.updateUser(user).observe(this, result -> {
            if (result == Result.SUCESS) {
                Toast.makeText(this, "User updated", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }


    private User getUser() {
        Bundle args = getIntent().getExtras();

        if (args != null && args.containsKey(Constant.BundleKey.USER)) {
            return Parcels.unwrap(args.getParcelable(Constant.BundleKey.USER));
        }

        return new User();
    }

    private boolean isEditable() {
        return binding.getModel().getId().equals(viewModel.getCurrentUser().getValue().getId());
    }
}
