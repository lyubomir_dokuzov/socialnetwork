package com.sample.socialnetwork.home.feed;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.LoggableResponseCallback;
import com.sample.socialnetwork.network.api.SocialNetworkApi;
import com.sample.socialnetwork.network.model.Post;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */

public class FeedRepositoryImpl implements FeedRepository {

    private final SocialNetworkApi socialNetworkApi;
    private final MutableLiveData<List<Post>> posts = new MutableLiveData<>();

    @Inject
    public FeedRepositoryImpl(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    @Override
    public LiveData<List<Post>> getUserPosts(String userId) {
        if (posts.getValue() == null) {
            fetchUserPosts(userId);
        }

        return posts;
    }

    @Override
    public LiveData<Result> post(Post post) {
        addAndUpdatedLocally(post);

        return submitNewPost(post);
    }

    @Override
    public LiveData<Result> deletePost(String postid) {
        removeAndUpdatedLocally(postid);

        return submitDeletePost(postid);
    }

    private void removeAndUpdatedLocally(String postId) {
        if (posts.getValue() != null) {

            List<Post> oldVal = Stream.of(posts.getValue())
                    .filter(value -> value != null && !value.getId().equals(postId))
                    .toList();

            posts.setValue(oldVal);
        }
    }

    private void addAndUpdatedLocally(Post post) {
        if (posts.getValue() != null) {
            List<Post> oldVal = posts.getValue();
            oldVal.add(post);

            posts.setValue(oldVal);
        }
    }

    private LiveData<Result> submitDeletePost(String postid) {
        MutableLiveData<Result> result = new MutableLiveData<>();
        socialNetworkApi.deletePost(postid).enqueue(new LoggableResponseCallback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                super.onResponse(call, response);
                result.setValue(Result.SUCESS);
            }
        });

        return result;
    }

    private MutableLiveData<Result> submitNewPost(Post post) {
        MutableLiveData<Result> result = new MutableLiveData<>();
        socialNetworkApi.submitPost(post).enqueue(new LoggableResponseCallback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                super.onResponse(call, response);
                result.setValue(Result.SUCESS);
            }
        });

        return result;
    }

    private void fetchUserPosts(String userId) {
        socialNetworkApi.getUserPosts(userId).enqueue(new LoggableResponseCallback<List<Post>>() {
            @Override
            public void onResponse(@NonNull Call<List<Post>> call, @NonNull Response<List<Post>> response) {
                super.onResponse(call, response);
                posts.setValue(response.body());
            }
        });
    }

}
