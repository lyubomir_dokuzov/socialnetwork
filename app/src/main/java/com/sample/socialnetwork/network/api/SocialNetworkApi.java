package com.sample.socialnetwork.network.api;

import com.sample.socialnetwork.network.model.Album;
import com.sample.socialnetwork.network.model.Note;
import com.sample.socialnetwork.network.model.Photo;
import com.sample.socialnetwork.network.model.Post;
import com.sample.socialnetwork.network.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Lyubomir Dokuzov
 */

public interface SocialNetworkApi {

    //region User

    @GET("/users/{userId}")
    Call<User> getUser(@Path("userId") String userId);

    @GET("/users")
    Call<List<User>> getUsers();

    @PUT("/users/{userId}")
    Call<Void> updateUser(@Path("userId") String userId, @Body User User); // In a better world,we'd use a PATCH
    //endregion

    //region Posts

    @GET("/posts")
    Call<List<Post>> getUserPosts(@Query("userId") String userId);

    @POST("/posts")
    Call<Void> submitPost(@Body Post post); // Generally REST returns created entity, but i'll use Void

    @DELETE("/posts/{postId}")
    Call<Void> deletePost(@Path("postId") String postId);
    //endregion

    //region Album/Photo

    @GET("/albums")
    Call<List<Album>> getUserAlbums(@Query("userId") String userId);

    @GET("/photos")
    Call<List<Photo>> getAlbumPhotos(@Query("albumId") String albumId);
    //endregion

    //region Notes

    @GET("/todos")
    Call<List<Note>> getUserNotes(@Query("userId") String userId);

    @POST("/todos")
    Call<Void> submitUserNote(@Body Note note);

    @DELETE("/todos/{noteId}")
    Call<Void> deleteUserNote(@Path("noteId") String noteId);

    //endregion
}
