package com.sample.socialnetwork.network;

import android.support.annotation.NonNull;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */

public abstract class LoggableResponseCallback<T> implements Callback<T> {


    private static final String NETWORK_ERROR = "Network error";
    private static final String NETWORK_RESPONSE = "Network response";


    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        Log.i(NETWORK_RESPONSE,
                "Status " + response.code() +
                        "\n url - " + call.request().url().toString()
        );
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        Log.i(NETWORK_ERROR, t.toString());
    }


}
