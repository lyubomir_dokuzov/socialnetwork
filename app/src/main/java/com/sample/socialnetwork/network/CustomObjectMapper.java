package com.sample.socialnetwork.network;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Lyubomir Dokuzov
 */
@Singleton
public class CustomObjectMapper extends ObjectMapper {

    @Inject
    public CustomObjectMapper() {
        disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    }
}
