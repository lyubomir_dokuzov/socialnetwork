package com.sample.socialnetwork.login;

import android.arch.lifecycle.LiveData;

import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.model.User;

import java.util.List;

/**
 * Created by Lyubomir Dokuzov
 */

public interface UserRepository {
    LiveData<List<User>> getUsers();

    LiveData<User> getCurrentlyLoggedUser();

    void logIn(String userId);

    LiveData<Result> updateUser(User user);
}
