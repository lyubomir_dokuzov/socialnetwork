package com.sample.socialnetwork.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.sample.socialnetwork.network.model.User;

import javax.inject.Inject;

/**
 * Created by Lyubomir Dokuzov
 */

public class LoginViewModel extends ViewModel {

    private final UserRepository userRepository;

    @Inject
    public LoginViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public void logIn(String userId) {
        userRepository.logIn(userId);
    }

    public LiveData<User> getCurrentlyLoggedInUser() {
        return userRepository.getCurrentlyLoggedUser();
    }
}
