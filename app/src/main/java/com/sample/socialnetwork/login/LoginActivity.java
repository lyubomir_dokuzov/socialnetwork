package com.sample.socialnetwork.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.sample.socialnetwork.R;
import com.sample.socialnetwork.base.BaseActivity;
import com.sample.socialnetwork.databinding.ActivityLoginBinding;
import com.sample.socialnetwork.home.SocialNetworkActivity;

/**
 * Created by Lyubomir Dokuzov
 */

public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding binding;
    private LoginViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel.class);
        viewModel.getCurrentlyLoggedInUser().observe(this, user -> navigateToUserHome());

        setupUi();
    }

    private void navigateToUserHome() {
        hideKeyboard();

        Intent intent = new Intent(this, SocialNetworkActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void setupUi() {
        binding.buttonLogin.setOnClickListener(view -> {
            String userId = binding.inputUserId.getText().toString();
            viewModel.logIn(userId);
        });
    }

}
