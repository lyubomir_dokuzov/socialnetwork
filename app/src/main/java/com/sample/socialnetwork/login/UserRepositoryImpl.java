package com.sample.socialnetwork.login;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.sample.socialnetwork.Result;
import com.sample.socialnetwork.network.LoggableResponseCallback;
import com.sample.socialnetwork.network.api.SocialNetworkApi;
import com.sample.socialnetwork.network.model.User;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Lyubomir Dokuzov
 */
public class UserRepositoryImpl implements UserRepository {

    private final SocialNetworkApi socialNetworkApi;

    private MutableLiveData<User> currentlyLoggedUser = new MutableLiveData<>();
    final MutableLiveData<List<User>> users = new MutableLiveData<>();

    @Inject
    public UserRepositoryImpl(SocialNetworkApi socialNetworkApi) {
        this.socialNetworkApi = socialNetworkApi;
    }

    @Override
    public LiveData<List<User>> getUsers() {
        if (users.getValue() == null) {
            fetchUsers(users);
        }

        return users;
    }

    @Override
    public void logIn(String userId) {
        fetchUserAndSetAsCurrent(userId);
    }

    @Override
    public LiveData<Result> updateUser(User user) {
        MutableLiveData<Result> result = new MutableLiveData<>();
        socialNetworkApi.updateUser(getCurrentlyLoggedUser().getValue().getId(), user).enqueue(new LoggableResponseCallback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                super.onResponse(call, response);
                result.setValue(Result.SUCESS);
            }
        });

        return result;
    }

    @Override
    public LiveData<User> getCurrentlyLoggedUser() {
        return currentlyLoggedUser;
    }

    private void fetchUsers(MutableLiveData<List<User>> data) {
        socialNetworkApi.getUsers().enqueue(new LoggableResponseCallback<List<User>>() {
            @Override
            public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                super.onResponse(call, response);
                if (response.body() == null) return;

                List<User> users = Stream.of(response.body())
                        .filter(user -> user != null && !user.getId().equals(getCurrentlyLoggedUser().getValue().getId()))
                        .toList();

                data.setValue(users);
            }
        });
    }

    private void fetchUserAndSetAsCurrent(String userId) {
        socialNetworkApi.getUser(userId).enqueue(new LoggableResponseCallback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                super.onResponse(call, response);
                if (response.body() != null) {
                    currentlyLoggedUser.setValue(response.body());
                }
            }
        });
    }

}
