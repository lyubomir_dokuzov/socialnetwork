package com.sample.socialnetwork;

import android.app.Application;
import android.content.Context;

import com.sample.socialnetwork.di.component.DaggerSocialNetworkComponent;
import com.sample.socialnetwork.di.component.SocialNetworkComponent;
import com.sample.socialnetwork.di.module.NetworkModule;
import com.sample.socialnetwork.di.module.SocialNetworkModule;

/**
 * Created by Lyubomir Dokuzov
 */

public class SocialNetworkApplication extends Application {

    private SocialNetworkComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerSocialNetworkComponent.builder()
                .socialNetworkModule(new SocialNetworkModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public static SocialNetworkComponent getAppComponent(Context context) {
        return ((SocialNetworkApplication) context.getApplicationContext()).appComponent;
    }

}
