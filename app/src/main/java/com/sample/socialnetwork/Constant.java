package com.sample.socialnetwork;

/**
 * Created by Lyubomir Dokuzov
 */

public final class Constant {


    public static class BundleKey {
        public static final String ALBUM_ID = "key_album_id";
        public static final String ALBUM_TITLE = "key_album_title";
        public static final String USER = "key_user";
    }

}
